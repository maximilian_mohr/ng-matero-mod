import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { SidebarComponent } from './admin-layout/sidebar/sidebar.component';
import { UserPanelComponent } from './admin-layout/sidebar/user-panel/user-panel.component';
import { SidemenuComponent } from './admin-layout/sidemenu/sidemenu.component';
import { AccordionAnchorDirective } from './admin-layout/sidemenu/accordionanchor.directive';
import { AccordionDirective } from './admin-layout/sidemenu/accordion.directive';
import { AccordionLinkDirective } from './admin-layout/sidemenu/accordionlink.directive';
import { SidebarNoticeComponent } from './admin-layout/sidebar-notice/sidebar-notice.component';
import { HeaderComponent } from './admin-layout/header/header.component';
import { BrandingComponent } from './admin-layout/header/branding/branding.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { UserComponent } from './admin-layout/header/user/user.component';
import { NotificationsComponent } from './admin-layout/header/notifications/notifications.component';
import { ScopeDetailsPanelComponent } from './admin-layout/sidebar/scope-details-panel/scope-details-panel.component';

@NgModule({
  declarations: [
    AdminLayoutComponent,
    SidebarComponent,
    UserPanelComponent,
    SidemenuComponent,
    AccordionAnchorDirective,
    AccordionDirective,
    AccordionLinkDirective,
    SidebarNoticeComponent,
    HeaderComponent,
    BrandingComponent,
    UserComponent,
    AuthLayoutComponent,
    NotificationsComponent,
    ScopeDetailsPanelComponent,
  ],
  imports: [SharedModule],
})
export class ThemeModule {}
