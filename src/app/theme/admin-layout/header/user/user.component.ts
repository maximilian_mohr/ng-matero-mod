import { Component, OnInit } from '@angular/core';
import { User, UserRole } from '@core/models';
import { UserService } from '@core/services/data';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userService.getLoggedInUser().subscribe(user => this.user = user);
  }

}
