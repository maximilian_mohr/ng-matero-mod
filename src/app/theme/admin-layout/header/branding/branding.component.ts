import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-branding',
  templateUrl: './branding.component.html',
  styleUrls: ['./branding.component.scss']
})
export class BrandingComponent implements OnInit {
  @Input() showLogo = true;
  @Input() showName = true;

  constructor() { }

  ngOnInit() {
  }

}
