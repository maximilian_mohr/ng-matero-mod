import { Component, OnInit } from '@angular/core';
import { NotificationService } from '@core/services/data';
import { Notification } from '@core/models';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notifications: Notification[];

  constructor(private notifService: NotificationService) { }

  ngOnInit() {
    this.getNotifications();
  }

  getNotifications() {
    this.notifService.getNotifications().subscribe(notifications => this.notifications = notifications);
  }
}
