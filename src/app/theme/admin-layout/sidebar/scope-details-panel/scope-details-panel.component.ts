import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ScopeDetails } from '@core/models/ui';
import { UiScopeService } from '@core/services';

@Component({
  selector: 'app-scope-details-panel',
  templateUrl: './scope-details-panel.component.html',
  styleUrls: ['./scope-details-panel.component.scss']
})
export class ScopeDetailsPanelComponent implements OnDestroy {

  /** The details data of the current UI scope */
  details: ScopeDetails;

  /** Subscription object used to unsubscribe on destruction */
  private dataSub: Subscription;


  constructor(private scopeService: UiScopeService) {
    this.dataSub = scopeService.scope$
      .subscribe(scope => this.details = scope.details);
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.dataSub.unsubscribe();
  }

  /** Set the current scope back to the default scope */
  resetScope() {
    this.scopeService.resetScope();
  }

}
