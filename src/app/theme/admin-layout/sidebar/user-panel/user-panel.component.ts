import { User } from '@core/models';
import { Component, OnInit } from '@angular/core';
import { UserService } from '@core/services/data';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {

  user: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userService.getLoggedInUser().subscribe(user => this.user = user);
  }
}
