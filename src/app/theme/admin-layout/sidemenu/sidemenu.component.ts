import { Component, Input, OnDestroy } from '@angular/core';
import { MenuService, Menu, UiScopeService } from '@core/services';
import { Subscription } from 'rxjs';
import { UiScope } from '@core/models/ui';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
})
export class SidemenuComponent implements OnDestroy {
  // NOTE: Ripple effect make page flashing on mobile
  @Input() ripple = true;

  /** The menu data of the current UI scope */
  menu: Menu;

  /** Subscription object for current menu data used to unsubscribe on destruction */
  private menuSub: Subscription;

  /** The data of the current UI scope */
  scope: UiScope;

  /** Subscription object for current scope used to unsubscribe on destruction */
  private scopeSub: Subscription;



  constructor(
    private menuService: MenuService,
    private scopeService: UiScopeService) {

    this.menuSub = menuService.currentMenu$
      .subscribe(menu => this.menu = menu);

    this.scopeSub = scopeService.scope$
      .subscribe(scope => this.scope = scope);
  }

  // Delete empty value in array
  filterStates(states: string[]) {
    return states.filter(item => item && item.trim());
  }

  ngOnDestroy() {
    this.menuSub.unsubscribe();
    this.scopeSub.unsubscribe();
  }
}
