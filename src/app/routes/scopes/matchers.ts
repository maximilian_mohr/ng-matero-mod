// import { UrlSegment, UrlSegmentGroup, Route } from '@angular/router';
// import { appInjector } from '../../../main';
// import { UiScopeService } from '@core/services';

// function matcher(
//   segments: UrlSegment[],
//   group: UrlSegmentGroup,
//   route: Route,
//   scopeName: string) {

//     const scopeService = appInjector.get(UiScopeService);
//     const isPathMatch = segments[0].path === route.path;
//     const isScopeTypeMatch = scopeService.getScope().name === scopeName;

//     if (isPathMatch && isScopeTypeMatch) {
//       return { consumed: [segments[0]]};
//     } else {
//       return null;
//     }
//   }

// export function xdgMatcher(
//    segments: UrlSegment[],
//    group: UrlSegmentGroup,
//    route: Route ) {
//      return matcher(segments, group, route, 'xdg');
//   }

// export function defaultMatcher(
//   segments: UrlSegment[],
//   group: UrlSegmentGroup,
//   route: Route ) {
//     return matcher(segments, group, route, 'default');
//   }
