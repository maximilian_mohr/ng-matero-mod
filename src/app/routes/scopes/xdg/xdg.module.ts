import { NgModule } from '@angular/core';

import { XdgRoutingModule } from './xdg-routing.module';
import { SharedModule } from '@shared';


@NgModule({
  declarations: [],
  imports: [
    SharedModule,
    XdgRoutingModule
  ]
})
export class XdgModule { }
