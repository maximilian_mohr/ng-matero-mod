import { NgModule } from '@angular/core';

import { SoftwareUpdateRoutingModule } from './software-update-routing.module';
import { SharedModule } from '@shared';
import { SoftwareUpdateComponent } from './software-update/software-update.component';


@NgModule({
  declarations: [SoftwareUpdateComponent],
  imports: [
    SharedModule,
    SoftwareUpdateRoutingModule
  ]
})
export class SoftwareUpdateModule { }
