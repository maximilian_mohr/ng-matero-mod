import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SoftwareUpdateComponent } from './software-update/software-update.component';



const routes: Routes = [
  {
    path: '',
    component: SoftwareUpdateComponent,
    data: { title: 'Software Update', titleI18n: 'Software Update' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoftwareUpdateRoutingModule { }
