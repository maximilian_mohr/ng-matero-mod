import { Component, OnInit } from '@angular/core';
import { FileValidator } from 'ngx-material-file-input';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-software-update',
  templateUrl: './software-update.component.html',
  styleUrls: ['./software-update.component.scss']
})
export class SoftwareUpdateComponent implements OnInit {


    fileUploadForm: FormGroup;

    /**
     * In this example, it's 100 MB (=100 * 2 ** 20).
     */
    readonly maxSize = 104857600;

    constructor(private fb: FormBuilder) {}

    ngOnInit() {
      this.fileUploadForm = this.fb.group({
        requiredfile: [
          undefined,
          [Validators.required, FileValidator.maxContentSize(this.maxSize)]
        ]
      });
    }

}
