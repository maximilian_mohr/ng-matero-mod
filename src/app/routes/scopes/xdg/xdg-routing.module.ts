import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'status',
    loadChildren: () => import ('./status/status.module').then(m => m.StatusModule),
  },
  {
    path: 'software-update',
    loadChildren: () => import ('./software-update/software-update.module').then(m => m.SoftwareUpdateModule),
  },
  {
    path: '',
    redirectTo: 'status',
    pathMatch: 'full'
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XdgRoutingModule { }
