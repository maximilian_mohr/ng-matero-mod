import { Component, OnInit } from '@angular/core';
import { XdgStatus } from '@core/models/xdg';
import { XdgStatusService } from '@core/services/data/xdg/xdg-status.service';

@Component({
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {

  status: XdgStatus;

  constructor(private statusService: XdgStatusService) { }

  ngOnInit() {
    this.getStatus();
  }

  private getStatus() {
    this.statusService.getStatus().subscribe(status => this.status = status);
  }

}
