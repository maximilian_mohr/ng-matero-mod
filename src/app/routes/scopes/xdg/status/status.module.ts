import { NgModule } from '@angular/core';

import { StatusRoutingModule } from './status-routing.module';
import { StatusComponent } from './status/status.component';
import { SharedModule } from '@shared';


@NgModule({
  declarations: [StatusComponent],
  imports: [
    SharedModule,
    StatusRoutingModule
  ]
})
export class StatusModule { }
