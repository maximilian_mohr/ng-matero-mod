import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlantOverviewComponent } from './plant-overview/plant-overview.component';


const routes: Routes = [
  {
    path: '',
    component: PlantOverviewComponent,
    data: { title: 'Plant Overview', titleI18n: 'Plant Overview' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlantOverviewRoutingModule { }
