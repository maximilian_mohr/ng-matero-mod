import { SelectedDeviceService } from './../selected-device.service';
import { Subscription } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';
import { BaseDevice } from '@core/models/device/base-device';
import { DeviceService } from '@core/services/data';
import { UiScopeService } from '@core/services';
import { ScopeLevel } from '@core/models/ui';
import { Router } from '@angular/router';

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss']
})
export class DeviceDetailsComponent implements OnDestroy {

  device: BaseDevice;

  private subscription: Subscription;


  constructor(private selectDeviceService: SelectedDeviceService,
              private scopeService: UiScopeService,
              private router: Router) {

    this.subscription = selectDeviceService.selectedDevice$.subscribe(
      device => {
        this.device = device;
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
  }

  close() {
    this.selectDeviceService.deselect();
  }

  select() {
    this.scopeService.setScope({name: this.device.type.toLowerCase(),
                                level: ScopeLevel.Device,
                                key: this.device.key,
                                details: {title: this.device.type,
                                          subTitle: `${this.device.key} - ${this.device.name}`
                                        }
                                      });
    this.router.navigate(['xdg/status']);
  }
}
