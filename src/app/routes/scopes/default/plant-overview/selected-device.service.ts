import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { BaseDevice } from '@core/models/device/base-device';

@Injectable({
  providedIn: 'root'
})
export class SelectedDeviceService {


  // Observable device sources
  private selectedDevice = new Subject<BaseDevice>();

  // Observable device streams
  selectedDevice$ = this.selectedDevice.asObservable();

  constructor() { }

  selectDevice(device: BaseDevice) {
    this.selectedDevice.next(device);
  }

  deselect() {
    this.selectedDevice.next(null);
  }
}
