import { NgModule } from '@angular/core';

import { PlantOverviewRoutingModule } from './plant-overview-routing.module';
import { MapComponent } from './map/map.component';
import { SharedModule } from '@shared';
import { PlantOverviewComponent } from './plant-overview/plant-overview.component';
import { DeviceDetailsComponent } from './device-details/device-details.component';
import { DeviceItemComponent } from './map/device-item/device-item.component';


@NgModule({
  declarations: [MapComponent, PlantOverviewComponent, DeviceDetailsComponent, DeviceItemComponent],
  imports: [
    SharedModule,
    PlantOverviewRoutingModule
  ]
})
export class PlantOverviewModule { }
