import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SelectedDeviceService } from '../../selected-device.service';
import { Subscription } from 'rxjs';
import { BaseDevice } from '@core/models/device/base-device';

@Component({
  selector: 'app-device-item',
  templateUrl: './device-item.component.html',
  styleUrls: ['./device-item.component.scss']
})
export class DeviceItemComponent implements OnInit, OnDestroy {

  // device data is passed in by template
  @Input() device: BaseDevice;

  // initial position is 0 (gets updated in ngInit)
  dragPosition = {x: 0, y: 0};

  // subscription for selected device changes
  private subscription: Subscription;


  constructor(public selectDeviceService: SelectedDeviceService) {
    this.subscription = selectDeviceService.selectedDevice$.subscribe(
      device => {
        // set the selected property
          this.device.dragDrop.selected = this.device === device;
        }
    );
  }

  ngOnInit() {
    this.setPosition();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
  }


  /**
   * Set the drag position to the last stored coordinates of the device
   *
   * @memberof DeviceItemComponent
   */
  setPosition() {
    this.dragPosition = this.device.dragDrop.position;
  }


  /**
   * Handler for the DragEnd event
   * Calculate and store the new position coordinates where the element was dropped.
   *
   * @param {*} $event  CdkDragEnd event
   * @memberof DeviceItemComponent
   */
  dragEnded($event) {
    let element = $event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    // determine parent position
    let x = 0;
    let y = 0;
    while(element && !isNaN(element.offsetLeft) && !isNaN(element.offsetTop)) {
      x += element.offsetLeft - element.scrollLeft;
      y += element.offsetTop - element.scrollTop;
      element = element.offsetParent;
    }
    const parentPosition = { top: y, left: x };

    // calculate and store X and Y position coordinates
    this.device.dragDrop.position.x = boundingClientRect.x - parentPosition.left;
    this.device.dragDrop.position.y = boundingClientRect.y - parentPosition.top;
  }

}
