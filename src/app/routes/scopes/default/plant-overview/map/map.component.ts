import { Component, OnInit } from '@angular/core';
import { SelectedDeviceService } from '../selected-device.service';
import { DeviceService } from '@core/services/data';
import { BaseDevice } from '@core/models/device/base-device';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  constructor(public selectDeviceService: SelectedDeviceService,
              private deviceService: DeviceService) { }

  devices: BaseDevice[];

  ngOnInit() {
    this.getDevices();
  }

  getDevices() {
    this.deviceService.getDevices().subscribe(devices => this.devices = devices);
  }
}
