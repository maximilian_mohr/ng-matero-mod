import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { TitleService } from '@core/services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
  
  constructor(private titleService: TitleService,
    private cdr: ChangeDetectorRef) {
    this.titleService.setPageTitle('Dashboard');
  }
  
  ngOnInit() {}
}
