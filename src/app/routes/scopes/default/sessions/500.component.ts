import { Component, OnInit } from '@angular/core';
import { TitleService } from '@core/services';

@Component({
  selector: 'app-error-500',
  template: `
    <error-code
      code="500"
      [title]="'Server went wrong!'"
      [message]="
        'Just kidding, looks like we have an internal issue, please try refreshing.'
      "
    >
    </error-code>
  `,
})
export class Error500Component implements OnInit {

  constructor(private titleService: TitleService) {
    this.titleService.setPageTitle('Error 500');
  }

  ngOnInit() {}
}
