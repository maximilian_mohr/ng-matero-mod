import { Component, OnInit } from '@angular/core';
import { TitleService } from '@core/services';

@Component({
  selector: 'app-error-403',
  template: `
    <error-code
      code="403"
      [title]="'Permission denied!'"
      [message]="'You do not have permission to access the requested data.'"
    ></error-code>
  `,
})
export class Error403Component implements OnInit {
  
  constructor(private titleService: TitleService) {
    this.titleService.setPageTitle('Error 403');
  }

  ngOnInit() {}
}
