import { Component, OnInit } from '@angular/core';
import { TitleService } from '@core/services';

@Component({
  selector: 'app-error-404',
  template: `
    <error-code
      code="404"
      [title]="'Page not found!'"
      [message]="'This is not the web page you are looking for.'"
    ></error-code>
  `,
})
export class Error404Component implements OnInit {
  
  constructor(private titleService: TitleService) {
    this.titleService.setPageTitle('Error 404');
  }

  ngOnInit() {}
}
