import { NgModule } from '@angular/core';

import { DefaultRoutingModule } from './default-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '@shared';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    SharedModule,
    DefaultRoutingModule
  ]
})
export class DefaultModule { }
