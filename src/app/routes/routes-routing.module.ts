import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from '../theme/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from '../theme/auth-layout/auth-layout.component';
import { AuthGuard } from '@core/auth.guard';
import { UserRole } from '@core/models';
import { DeviceScopeGuard } from '@core/device-scope.guard';


const routes: Routes = [
  /*
      Page routes to pages which require authentication
  */
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    data: { role: UserRole.User },
    children: [
      {
        path: 'd',
        canActivate: [DeviceScopeGuard],
        children: [
          {
            path: 'xdg',
            loadChildren: () => import('./scopes/xdg/xdg.module').then(m => m.XdgModule)
          }
        ]
      },
      {
        path: 'g',
        loadChildren: () => import('./scopes/default/default.module').then(m => m.DefaultModule)
      },
    ],
  },

  /*
      Page routes to authentication pages
  */
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
      }
    ]
  },

  /*
      Basic redirect routes
  */
  {
    path: '',
    redirectTo: 'g',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
})
export class RoutesRoutingModule {}
