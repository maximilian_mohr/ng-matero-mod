import { NgModule } from '@angular/core';

import { AuthRoutingModule } from './auth-routing.module';
import { LogoutComponent } from './logout.component';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '@shared';


@NgModule({
  declarations: [LogoutComponent, LoginComponent],
  imports: [
    SharedModule,
    AuthRoutingModule
  ]
})
export class AuthModule { }
