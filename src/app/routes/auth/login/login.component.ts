import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TitleService, AuthService } from '@core/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private titleService: TitleService,
              private authService: AuthService) {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
    this.titleService.setPageTitle('Login');
  }

  ngOnInit() {
  }

  login() {
    const val = this.loginForm.value;

    if (val.username && val.password) {
      // TODO: Remove when Rest-API is available for authorization
      this.router.navigateByUrl('/');
      return;
      this.authService.login(val.username, val.password)
        .subscribe(
          () => {
            if (this.authService.isLoggedIn()) {
              this.router.navigateByUrl('/');
            } else {
              this.router.navigateByUrl('/login');
            }
          }
        );
    }
  }
}
