import { UiScope } from '@core/models/ui';
import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription  } from 'rxjs';
import { Router } from '@angular/router';
import { UiScopeService } from './ui-scope.service';

export interface Tag {
  color: string; // Background Color
  value: string;
}

export interface Menu {
  linkPrefix: string;
  items: MenuItem[];
}

export interface MenuItem {
  state: string;
  name: string;
  type: 'link' | 'sub' | 'extLink' | 'extTabLink';
  icon: string;
  label?: Tag;
  badge?: Tag;
  children?: MenuItem[];
}

@Injectable({
  providedIn: 'root',
})
export class MenuService implements OnDestroy {

  private scopeSub: Subscription;

  // Observable menu
  private currentMenu = new BehaviorSubject<Menu>({linkPrefix: '/', items: []});
  currentMenu$ = this.currentMenu.asObservable();

  constructor(private http: HttpClient,
              private scopeService: UiScopeService,
              private router: Router) {
    // subscribe for application scope changes
    this.scopeSub = this.scopeService.scope$.subscribe(
      scope => {
        // load and set the scope specific menu
        this.loadMenuByScope(scope);
      }
    );
  }

  ngOnDestroy() {
    this.scopeSub.unsubscribe();
  }

  public loadMenuByScope(scope: UiScope) {



    console.log('SCOPE CHANGED: ' + scope.name);
    // Set the API url to fetch the menu by the given scope
    // const url = `/api/ui/menu/${scope.name}`;

    // TODO: Remove assets url when API is ready (!)
    const url = `assets/data/menu_${scope.name}.json`;

    this.http.get<Menu>(url).subscribe(menu => {console.log(menu); this.set(menu);});
  }


  set(menu: Menu) {
    this.currentMenu.next(menu);
  }

  getMenuItemName(stateArr: string[]): string {
    return this.getMenuLevel(stateArr)[stateArr.length - 1];
  }

  getMenuLevel(stateArr: string[]): string[] {
    const tempArr = [];

    const menu = this.currentMenu.getValue();

    // url prefix for the active menu?
    const prefix = menu.linkPrefix !== '' ? menu.linkPrefix + '/' : '';

    this.currentMenu.getValue().items.map(item => {
      if (prefix + item.state === stateArr[0]) {
        tempArr.push(item.name);
        // Level1
        if (item.children && item.children.length) {
          item.children.forEach(itemlvl1 => {
            if (stateArr[1] && prefix + itemlvl1.state === stateArr[1]) {
              tempArr.push(itemlvl1.name);
              // Level2
              if (itemlvl1.children && itemlvl1.children.length) {
                itemlvl1.children.forEach(itemlvl2 => {
                  if (stateArr[2] && prefix + itemlvl2.state === stateArr[2]) {
                    tempArr.push(itemlvl2.name);
                  }
                });
              }
            } else if (stateArr[1]) {
              // Level2
              if (itemlvl1.children && itemlvl1.children.length) {
                itemlvl1.children.forEach(itemlvl2 => {
                  if (prefix + itemlvl2.state === stateArr[1]) {
                    tempArr.push(itemlvl1.name, itemlvl2.name);
                  }
                });
              }
            }
          });
        }
      }
    });
    return tempArr;
  }
}
