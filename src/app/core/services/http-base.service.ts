import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpBaseService {

  // Base URL for the child service implementation
  protected url: string;

  constructor(url: string) {
    // Setup the base URL with the common prefix "/api"
    this.url = '/api' + url;
  }
}
