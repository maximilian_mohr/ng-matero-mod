import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseDevice } from '@core/models/device/base-device';
import { DeviceCategory } from '@core/models/device/device-category.enum';
import { DeviceStatus } from '@core/models/device/device-status.enum';

const DEVICE_DATA: BaseDevice[] = [
  new BaseDevice('SHF1', 'XCO', DeviceCategory.ComNode, 'Lager-Zentrale', '10.60.123.1', DeviceStatus.Ready),
  new BaseDevice('CID1901', 'XDG', DeviceCategory.Equipment, 'HRL', '10.60.123.10', DeviceStatus.Ready),
  new BaseDevice('CID1921', 'XBC', DeviceCategory.Equipment, 'Lager-Außenleuchten', '10.60.123.12', DeviceStatus.NotConfigured),
  new BaseDevice('CID1101', 'NRO', DeviceCategory.Terminal, 'Lager-Empfang', '10.60.123.21', DeviceStatus.NeedRestart),
  new BaseDevice('CID1902', 'XDG', DeviceCategory.Equipment, 'Außenlager', '10.60.123.11', DeviceStatus.NotAvailable),
];


@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor() { }

  public getDevices(): Observable<BaseDevice[]> {
    // TODO: Remove when HTTP is working
    return of(DEVICE_DATA);
  }
}
