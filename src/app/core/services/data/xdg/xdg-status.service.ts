import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { XdgStatus } from '@core/models/xdg';

@Injectable({
  providedIn: 'root'
})
export class XdgStatusService {

  constructor() { }

  public getStatus(): Observable<XdgStatus> {

    // TODO: Remove when HTTP is working
    // Static test data for demo
    return of({software: {version: '1.0.0 Release', buildDate: '2019-10-16 10:22:35'}, role: {name: 'Außenlager', systemId: 1, systemNetworkIdentifier: 'mo_test'}});

  }
}
