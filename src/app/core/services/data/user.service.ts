import { Injectable } from '@angular/core';
import { HttpBaseService } from '../http-base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { User, UserRole } from '@core/models';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends HttpBaseService {

  constructor(private http: HttpClient,
              private auth: AuthService) {
    super('/users');
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.url}/${id}`);
  }

  public getLoggedInUser(): Observable<User> {

    // TODO: Remove when HTTP is working
    // Static test user for demo
    return of({id: 1, userName: 'MaxTest', role: UserRole.Admin, email: "max.mustermann@test.com", firstName: "Max", lastName: "Mustermann"});

    return this.getUserById(this.auth.getAuthenticatedUser().id);
  }
}
