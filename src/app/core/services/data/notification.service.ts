import { Injectable } from '@angular/core';
import { HttpBaseService } from '../http-base.service';
import { HttpClient } from '@angular/common/http';
import { Notification, NotificationLevel } from '@core/models';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService extends HttpBaseService {


  constructor(private http: HttpClient) {
    super('/notifications');
  }

  public getNotifications(): Observable<Notification[]> {

    // TODO: Remove when HTTP is working
    return of([
      {message: 'Server Error Reports', level: NotificationLevel.Warning},
      {message: 'New Data Received', level: NotificationLevel.Info},
      {message: 'Unexpected Shutdown Occured', level: NotificationLevel.Error},
    ]);

    return this.http.get<Notification[]>(this.url);
  }
}
