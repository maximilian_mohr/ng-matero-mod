import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupMessageService {

  constructor(private snackbar: MatSnackBar) { }

  /**
   * Open popup in success (green) style
   */
  public showSuccess(message: string, action?: string) {
    this.openSnackBar(message, action, 'success');
  }

  /**
   * Open popup in error (red) style
   */
  public showError(message: string, action?: string) {
    this.openSnackBar(message, action, 'error');
  }

  /**
   * Open popup in warning (green) style
   */
  public showWarning(message: string, action?: string) {
    this.openSnackBar(message, action, 'warning');
  }

  /**
   * Closes the current-visible popup
   */
  public close() {
    this.snackbar.dismiss();
  }


  private openSnackBar(message: string, action: string, panelClass: string) {

    this.snackbar.open(message, '', {
      panelClass,
      duration: 5000,
      verticalPosition: 'top',
      horizontalPosition: 'center'
    });
  }
}
