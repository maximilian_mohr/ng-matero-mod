
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UiScope } from '@core/models/ui';
import { ScopeLevel } from '@core/models/ui/scope-level.enum';

/** static default scope definition */
const DEFAULT_SCOPE: UiScope = {name: 'default', level: ScopeLevel.System, key: 'X'};

/**
 * Handles the current scope of the application's UI.
 *
 * The scope can be on
 * - device level, showing navigation and components handling on single devie
 * - whole system level with overview over multiple devices and other management stuff
 */
@Injectable({
  providedIn: 'root'
})
export class UiScopeService {

  // ######################################################
  //    Properties

  /** Subject containing the current scope data object */
  private scope = new BehaviorSubject<UiScope>(DEFAULT_SCOPE);

  /** Observable scope data */
  scope$ = this.scope.asObservable();

  // ######################################################
  //    Methods

  constructor() {
     const lastScope = localStorage.getItem('scope');
     if (lastScope) {
       this.setScope(JSON.parse(lastScope));
     } else {
      this.resetScope();
     }
  }

  /**
   * Reset the application UI scope to default.
   */
  public resetScope() {
    this.setScope(DEFAULT_SCOPE);
  }

  /**
   * Gets the current application UI scope.
   *
   * @returns Current scope object
   */
  public getScope(): UiScope {
    return this.scope.getValue();
  }

  /**
   * Change the application UI scope to the new one given.
   *
   * @param scope   The new scope to change to
   */
  public setScope(scope: UiScope) {
    this.scope.next(scope);
    localStorage.setItem('scope', JSON.stringify(scope));
  }
}
