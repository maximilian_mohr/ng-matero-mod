import { HttpBaseService } from './http-base.service';
import { tap, shareReplay, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '@core/models';
import { JwtService } from './jwt.service';
import { throwError, of } from 'rxjs';
import { PopupMessageService } from './popup-message.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends HttpBaseService {

  constructor(
    private http: HttpClient,
    private jwt: JwtService,
    private popup: PopupMessageService) {
      super('/authenticate');
    }

  login(userName: string, password: string ) {
    return this.http.post<any>(this.url, {userName, password})
      .pipe(
        tap(user => {
          if (user) {
            this.jwt.set(user.token);
            this.popup.showSuccess('Anmeldung erfolgreich!', 'OK');
          }
        }),
        shareReplay(),
        catchError(err => {
          // handle login failed (401 - Unauthorized)
          if (err.status === 401) {
            this.popup.showError('Anmeldung fehlgeschlagen! Name oder Passwort sind ungültig.');
            return of([]);
          } else {
            // just rethrow any unexpected error
            return throwError(err);
          }
      })
    );
  }

  getAuthenticatedUser(): User {
    if (this.isLoggedIn()) {
      const decodedJwt = this.jwt.getDecoded();
      if (!decodedJwt) { return null; }
      return { id: decodedJwt.nameid, userName: decodedJwt.unique_name, role: decodedJwt.role };
    }
    return null;
  }

  logout() {
    this.jwt.remove();
  }

  isLoggedIn() {
    return !this.jwt.isExpired();
  }

  isLoggedOut() {
      return !this.isLoggedIn();
  }
}
