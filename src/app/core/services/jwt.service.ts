import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

export const TOKEN_NAME = 'authJwt';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }

  get(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  set(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  remove(): void {
    localStorage.removeItem(TOKEN_NAME);
  }

  getDecoded(token?: string) {
    if (!token) { token = this.get(); }
    if (!token) { return null; }
    return jwt_decode(token);
  }

  isExpired(): boolean {
    const date = this.getExpirationDate();
    if(date === undefined || date == null) { return true; }
    return !(date.valueOf() > new Date().valueOf());
  }

  private getExpirationDate(): Date {

    const decoded = this.getDecoded();
    if (!decoded) { return null; }
    if (decoded.exp === undefined) { return null; }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }
}
