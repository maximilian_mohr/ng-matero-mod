import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  private titlePrefix = '';
  private pageTitle = '';
  private titleSuffix = 'NgMateroMod'

  constructor(private title: Title) { }

  setPageTitle(pageTitle: string) {
    this.pageTitle = pageTitle;
    this.updateTitle();
  }

  getPageTitle(): string {
    return this.pageTitle;
  }

  setTitlePrefix(prefix: string) {
    this.titlePrefix = prefix;
    this.updateTitle();
  }

  private updateTitle() {
    // build the page title
    var title = this.pageTitle;

    // add prefix with space if not empty
    if (this.titlePrefix !== '') {
      title = this.titlePrefix + ' ' + title;
    }
    
    // add suffix with pipe if not empty
    if (this.titleSuffix !== '') {
      title += ' | ' + this.titleSuffix;
    }

    this.title.setTitle(title);
  }
}
