import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UiScopeService } from './services';
import { ScopeLevel } from './models/ui';

@Injectable({
  providedIn: 'root'
})
export class DeviceScopeGuard implements CanActivate {

  constructor(private scopeService: UiScopeService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const scope = this.scopeService.getScope();

      return scope.level === ScopeLevel.Device;
  }
}
