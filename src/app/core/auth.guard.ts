import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './services';
import { UserRole } from './models';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,
              private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      // TODO: Remove when Rest-API is available for authorization
      return true;
      if (this.authService.isLoggedIn()) {
        const user = this.authService.getAuthenticatedUser();
        if (user) {
          return true;
          // TODO: Enable role handling if needed
          if (route.data.role) {
            switch (user.role) {
              case UserRole.Admin:
                return true;

              case UserRole.User:
                  return route.data.role === UserRole.User;
            }
          }
        }
      }
      this.authService.logout();
      this.router.navigate(['/login']);
      return false;
  }

}
