import { NotificationLevel } from './notification-level.enum';

export interface Notification {
    message: string;
    level: NotificationLevel;
}
