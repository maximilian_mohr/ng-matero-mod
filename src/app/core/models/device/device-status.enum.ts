/**
 * Simple enumeration to define the current status of the device.
 */
export enum DeviceStatus {
  NotAvailable = 'Not available',
  NotConfigured = 'Not configured',
  Ready = 'Betriebsbereit',
  NeedRestart = 'Neustart erforderlich'
}
