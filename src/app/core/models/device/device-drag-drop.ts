export interface DeviceDragDrop {

  // Store X and Y coordinates for drag-drop functionality
  position: {
    x: number,
    y: number
  }

  // Store selected property when the drag-drop item is selected
  selected: boolean;
}
