/**
 * Enumeration to define the category of a device
 */
export enum DeviceCategory {
  ComNode = 'Kommunikationsknoten',
  Terminal = 'Station',
  Equipment = 'Equipment',
  Amp = 'Verstärker'
}
