import { DeviceDragDrop } from './device-drag-drop';
import { DeviceStatus } from './device-status.enum';
import { DeviceCategory } from './device-category.enum';

/**
 * Represents basic and generic device data for several generic view purposes
 */
export class BaseDevice {

  // optional needed drag-drop properties
  dragDrop: DeviceDragDrop = {
    position: {
      x: 0, y: 0
    },
    selected: false
  };

  constructor(public key: string,
              public type: string,
              public category: DeviceCategory,
              public name: string,
              public ipAddress: string,
              public status: DeviceStatus) { }

  /**
   * Determine the color theme name depending on the current device status
   *
   * @returns {string} color theme name
   * @memberof Device
   */
  getStatusBadgeColor(): string {
    switch (this.status) {
      case DeviceStatus.NotAvailable:
      case DeviceStatus.NotConfigured:
        return 'warn';

      case DeviceStatus.NeedRestart:
        return 'accent';

      case DeviceStatus.Ready:
        return '';
    }
  }

  /**
   * Get the status badge text.
   * Empty string, when no badge is needed (!)
   *
   * @returns {string} The badge text/character
   * @memberof Device
   */
  getStatusBadgeText(): string {
    return this.status !== DeviceStatus.Ready ? '!' : '';
  }

  getCategoryIcon(): string {
    switch (this.category) {
      case DeviceCategory.ComNode:
        return 'device_hub';

      case DeviceCategory.Equipment:
        return 'developer_board';

      case DeviceCategory.Amp:
        return 'speaker';

      case DeviceCategory.Terminal:
        return 'scanner';
    }
  }

  getStatusTextClass(): string {
    switch (this.status) {
      case DeviceStatus.NotAvailable:
        return 'text-red-900';

      case DeviceStatus.NotConfigured:
        return 'text-orange-900';

      case DeviceStatus.NeedRestart:
        return 'text-teal-500';

      case DeviceStatus.Ready:
        return 'text-green-900';

    }
  }

}

