export interface XdgStatus {

  // XDG software info
  software: {
    version: string,
    buildDate: string
  };

  // XDG Ip role information
  role: {
    name: string,
    systemId: number,
    systemNetworkIdentifier: string
  };

}
