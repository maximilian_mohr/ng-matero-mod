import { ScopeDetails } from './scope-details';
import { ScopeLevel } from './scope-level.enum';

/**
 * Represents a UI scope and contains all data which is needed to handle
 * the current application scope.
 */
export interface UiScope {

  /** Simple descriptive name of the scope */
  name: string;

  /** The scope level */
  level: ScopeLevel;

  /** Details data for this scope */
  details?: ScopeDetails;

  /** A unique key to identify this scope (and the device behind) */
  key: string;

  /** Prefix which is added between "/api/" and the request url when HTTP requests are done */
  apiUrlPrefix?: string;
}
