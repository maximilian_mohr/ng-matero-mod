export enum ScopeLevel {
  System = 'System',
  Device = 'Device'
}
