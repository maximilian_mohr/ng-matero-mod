
/** Details data for a UI scope. Used to display data on frontend */
export interface ScopeDetails {

  /** The title of the scope to be shown e.g. in sidebar */
  title?: string;

  /** The sub title of the scope to be shown e.g. in sidebar */
  subTitle?: string;
}
