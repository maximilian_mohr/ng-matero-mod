import { UserRole } from './user-role.enum';

export interface User {
    id: number;         // unique user ID
    userName: string;   // the login user name
    role: UserRole;     // the role for authorizing access

    /* Person and contact data (optional data, only provided by API request) */
    firstName?: string;
    lastName?: string;
    email?: string;
}
